package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Admin;
import model.Loanee;
import util.DbUtil;
import util.QueryUtil;

public class LoaneeDaoImpl implements LoaneeDao {
	PreparedStatement pst;
	ResultSet rs;
	int result;

	public int adminLogin(Admin admin) {
		result = 0;
		try {
			pst = DbUtil.getConnection().prepareStatement(QueryUtil.adminLogin);
			pst.setString(1, admin.getUname());
			pst.setString(2, admin.getPassword());
			rs = pst.executeQuery();
			while (rs.next()) {
				result++;
			}
		} catch (ClassNotFoundException | SQLException e) {

		}
		return result;
	}

	public List<Loanee> viewAllLoanee() {
		List<Loanee> list = new ArrayList<Loanee>();
		try {
			pst = DbUtil.getConnection().prepareStatement(QueryUtil.viewAll);
			rs = pst.executeQuery();
			while (rs.next()) {
				Loanee loanee = new Loanee(rs.getInt("lid"), rs.getString(2), rs.getLong("amt"), rs.getInt("rateofin"),
						rs.getInt("years"), rs.getFloat("interest"));
				list.add(loanee);
			}
		} catch (ClassNotFoundException | SQLException e) {

		}
		return list;
	}

	public int add(Loanee loanee) {
		result = 0;
		try {
			pst = DbUtil.getConnection().prepareStatement(QueryUtil.add);
			pst.setInt(1, loanee.getLid());
			pst.setString(2, loanee.getLname());
			pst.setLong(3, loanee.getAmt());
			pst.setInt(4, loanee.getRateofin());
			pst.setInt(5, loanee.getYears());
			pst.setFloat(6, loanee.getInterest());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			return result;
		}

		return result;
	}

	public void update(Loanee loanee) {

		try {
			pst = DbUtil.getConnection().prepareStatement(QueryUtil.update);
			pst.setString(1, loanee.getLname());
			pst.setLong(2, loanee.getAmt());
			pst.setInt(3, loanee.getRateofin());
			pst.setInt(4, loanee.getYears());
			pst.setFloat(5, loanee.getInterest());
			pst.setInt(6, loanee.getLid());
			pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {

		}

	}

	public void delete(Loanee loanee) {
		try {
			pst = DbUtil.getConnection().prepareStatement(QueryUtil.delete);
			pst.setInt(1, loanee.getLid());
			pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {

		}
	}
}
