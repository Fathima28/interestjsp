package dao;

import java.util.List;

import model.Admin;
import model.Loanee;

public interface LoaneeDao {
	public int adminLogin(Admin admin);

	public List<Loanee> viewAllLoanee();

	public int add(Loanee loanee);

	public void update(Loanee loanee);

	public void delete(Loanee loanee);

}
