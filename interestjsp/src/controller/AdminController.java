package controller;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.LoaneeDao;
import dao.LoaneeDaoImpl;
import model.Admin;
import model.Loanee;

@WebServlet(name = "AdminController", urlPatterns = { "/login", "/add", "/updateCont" })
public class AdminController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url = request.getServletPath();
		LoaneeDao dao = new LoaneeDaoImpl();
		if (url.equals("/login")) {
			String uname = request.getParameter("uname");
			String password = request.getParameter("password");
			HttpSession session = request.getSession(true);
			session.setAttribute("uname", uname);
			System.out.println(uname + "Logged in @ " + new Date(session.getCreationTime()));
			Admin admin = new Admin(uname, password);
			int result = dao.adminLogin(admin);
			if (result > 0) {
				List<Loanee> list = dao.viewAllLoanee();
				request.setAttribute("loan", list);
				request.getRequestDispatcher("admin.jsp").forward(request, response);
			} else {
				request.setAttribute("error", "Hai " + uname + ",Please Check your Username and Password");
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
		} else if (url.equals("/add")) {
			int lid = Integer.parseInt(request.getParameter("lid"));
			String lname = request.getParameter("lname");
			long amt = Long.parseLong(request.getParameter("amt"));
			int rateofin = Integer.parseInt(request.getParameter("rateofin"));
			int years = Integer.parseInt(request.getParameter("years"));
			float interest = Float.parseFloat(request.getParameter("interest"));
			Loanee loanee = new Loanee(lid, lname, amt, rateofin, years, interest);
			int result = dao.add(loanee);
			if (result > 0) {
				request.setAttribute("msg", lid + " inserted successfully");
			} else {
				request.setAttribute("msg", lid + "duplicate entry");
			}
			List<Loanee> list = dao.viewAllLoanee();
			request.setAttribute("loan", list);
			request.getRequestDispatcher("adminCrud.jsp").forward(request, response);
		} else if (url.equals("/updateCont")) {
			String lname = request.getParameter("lname");
			long amt = Long.parseLong(request.getParameter("amt"));
			int rateofin = Integer.parseInt(request.getParameter("rateofin"));
			int years = Integer.parseInt(request.getParameter("years"));
			float interest = Float.parseFloat(request.getParameter("interest"));
			int lid = Integer.parseInt(request.getParameter("lid"));
			Loanee loanee = new Loanee(lid, lname, amt, rateofin, years, interest);
			dao.update(loanee);
			List<Loanee> list = dao.viewAllLoanee();
			request.setAttribute("loan", list);
			request.getRequestDispatcher("adminCrud.jsp").include(request, response);
		}
	}
}
