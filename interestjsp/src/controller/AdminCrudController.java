package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.LoaneeDao;
import dao.LoaneeDaoImpl;
import model.Loanee;

@WebServlet(name = "AdminCrudController", urlPatterns = { "/adminCrud", "/home", "/delete", "/insert", "/signout",
		"/update" })
public class AdminCrudController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url = request.getServletPath();
		LoaneeDao dao = new LoaneeDaoImpl();
		Loanee loanee = new Loanee();

		if (url.equals("/adminCrud")) {
			List<Loanee> list = dao.viewAllLoanee();
			request.setAttribute("loan", list);
			request.getRequestDispatcher("adminCrud.jsp").include(request, response);
		} else if (url.equals("/home")) {
			List<Loanee> list = dao.viewAllLoanee();
			request.setAttribute("loan", list);
			request.getRequestDispatcher("admin.jsp").forward(request, response);
		} else if (url.equals("/delete")) {
			int lid = Integer.parseInt(request.getParameter("lid"));
			loanee.setLid(lid);
			dao.delete(loanee);
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			out.print(lid + " deleted successfully<br/>");
			List<Loanee> list = dao.viewAllLoanee();
			request.setAttribute("loan", list);
			request.getRequestDispatcher("adminCrud.jsp").include(request, response);
		} else if (url.equals("/insert")) {
			request.getRequestDispatcher("insert.jsp").forward(request, response);
		} else if (url.equals("/signout")) {
			HttpSession session = request.getSession(false);
			String uname = (String) session.getAttribute("uname");
			System.out.println(uname + " logged out @ " + new Date());
			session.invalidate();
			request.setAttribute("uname", uname + " logged out successfully!!!");
			request.getRequestDispatcher("index.jsp").forward(request, response);
		} else if (url.equals("/update")) {
			String lname = request.getParameter("lname");
			long amt = Long.parseLong(request.getParameter("amt"));
			int rateofin = Integer.parseInt(request.getParameter("rateofin"));
			int years = Integer.parseInt(request.getParameter("years"));
			float interest = Float.parseFloat(request.getParameter("interest"));
			int lid = Integer.parseInt(request.getParameter("lid"));
			Loanee loanee1 = new Loanee(lid, lname, amt, rateofin, years, interest);
			request.setAttribute("loan", loanee1);
			request.getRequestDispatcher("update.jsp").forward(request, response);
		}
	}
}
