<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update</title>
<link rel="stylesheet" type="text/css" href="form.css">
</head>
<body>
	<h1>Admin Update Page</h1>
	Hai,
	<%=(String) session.getAttribute("uname")%><hr />

	<form action="updateCont" method="post">
		<table>
			<tr>
				<td>LoaneeID:</td>
				<td><input type="text" name="lid" readonly="readonly"
					value="${loan.getLid()}" /></td>
			</tr>
			<tr>
				<td>LoaneeName:</td>
				<td><input type="text" name="lname" value="${loan.getLname()}" /></td>
			</tr>
			<tr>
				<td>Amount:</td>
				<td><input type="text" name="amt" value="${loan.getAmt()}" /></td>
			</tr>
			<tr>
				<td>RateOfInterest:</td>
				<td><input type="text" name="rateofin"
					value="${loan.getRateofin()}" /></td>
			</tr>
			<tr>
				<td>Years:</td>
				<td><input type="text" name="years" value="${loan.getYears()}" /></td>
			</tr>
			<tr>
				<td>Interest:</td>
				<td><input type="text" name="interest"
					value="${loan.getInterest()}" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="Update" /></td>
			</tr>
		</table>

	</form>

</body>
</html>