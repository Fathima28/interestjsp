<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>AdminCRUD</title>
<link rel="stylesheet" type="text/css" href="form.css">
</head>
<body>
	<h1>Admin CRUD Operations Page</h1>
	<p align="right">
		Hai,<%=(String) session.getAttribute("uname")%>
		<a href="home"> Home</a> <a href="insert"> AddNewLoanee</a> <a
			href="signout">Logout</a>
	</p>
	<hr />
	${msg}
	<table border="5">
		<tr>
			<th colspan="8">LoaneeDetails</th>
		</tr>
		<tr>
			<th>LoaneeID</th>
			<th>LoaneeName</th>
			<th>Amount</th>
			<th>RateOfInterest</th>
			<th>Years</th>
			<th>Interest</th>
			<th>Update</th>
			<th>Delete</th>
		</tr>
		<c:forEach items="${loan}" var="l">
			<tr style="text-align: center;">
				<td>${l.getLid()}</td>
				<td>${l.getLname()}</td>
				<td>${l.getAmt()}</td>
				<td>${l.getRateofin()}</td>
				<td>${l.getYears()}</td>
				<td>${l.getInterest()}</td>
				<td><a style="text-decortaion: none"
					href="update?lid=${l.getLid()}&lname=${l.getLname()}&amt=${l.getAmt()}&rateofin=${l.getRateofin()}&years=${l.getYears()}&interest=${l.getInterest()}">Update</a></td>
				<td><a style="text-decoration: none"
					href="delete?lid=${l.getLid()}">Delete</a></td>
		</c:forEach>
	</table>
</body>
</html>