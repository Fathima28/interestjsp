<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Welcome</title>
<link rel="stylesheet" type="text/css" href="form.css">
</head>
<body>
	<h1>Welcome to Home Page</h1>
	<hr />
	${uname}
	<form action="login" method="post">
		${error} <br />
		<table>
			<tr>
				<td>UserName:</td>
				<td><input type="text" name="uname" autocomplete="off" /></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type="password" name="password" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="login" /></td>
			</tr>
		</table>
	</form>


</body>
</html>