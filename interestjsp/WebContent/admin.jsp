<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin</title>
<link rel="stylesheet" type="text/css" href="form.css">
</head>
<body>
	<h1>Admin Home Page</h1>
	<p align="right">
		Hai,<%=(String) session.getAttribute("uname")%>
		<a href="adminCrud"> AdminCRUD</a> <a href="signout">Logout</a>
	</p>
	<hr />
	<table border="5">
		<tr>
			<th colspan="6">Loanee Deatils</th>
		</tr>
		<tr>
			<th>LoaneeID</th>
			<th>Loanee Name</th>
			<th>Amount</th>
			<th>RateOfInterest</th>
			<th>Years</th>
			<th>Interest</th>
		</tr>
		<c:forEach items="${loan}" var="l">
			<tr style="text-align: center;">
				<td>${l.getLid()}</td>
				<td>${l.getLname()}</td>
				<td>${l.getAmt()}</td>
				<td>${l.getRateofin()}</td>
				<td>${l.getYears()}</td>
				<td>${l.getInterest()}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>