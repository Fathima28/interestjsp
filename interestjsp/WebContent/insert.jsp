<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Loanee</title>
<link rel="stylesheet" type="text/css" href="form.css">
</head>
<body>
	<h1>Add New Loanee Details</h1>
	Hai,<%=(String) session.getAttribute("uname")%>
	<hr />
	<form action="add" method="post">
		<table>
			<tr>
				<td>LoaneeId:</td>
				<td><input type="text" name="lid" /></td>
			</tr>
			<tr>
				<td>LoaneeName:</td>
				<td><input type="text" name="lname" /></td>
			</tr>
			<tr>
				<td>Amount:</td>
				<td><input type="text" name="amt" /></td>
			</tr>
			<tr>
				<td>Rate of Interest:</td>
				<td><input type="text" name="rateofin" /></td>
			</tr>
			<tr>
				<td>Years:</td>
				<td><input type="text" name="years" /></td>
			</tr>
			<tr>
				<td>Interest:</td>
				<td><input type="text" name="interest" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="Add" /></td>
			</tr>
		</table>
	</form>

</body>
</html>